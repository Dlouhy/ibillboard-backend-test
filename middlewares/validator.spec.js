'use strict';

const assert = require('chai').assert;
const sinon = require('sinon');

const prepareFakeHTTP = require('../tests/prepareFakeHTTP');
const validator = require('./validator');

const correctBody = { body: {} };
const incorrectBody = 'Hello';

describe('validator middleware', function() {
    describe('uses method isBodyObject', function() {
        beforeEach(function() {
            this.fakeHTTP = prepareFakeHTTP();
        });

        it('with correct body type.', function() {
            this.fakeHTTP.req = correctBody;

            validator.isBodyObject(this.fakeHTTP.req, this.fakeHTTP.res, this.fakeHTTP.next);

            assert.isTrue(this.fakeHTTP.next.called);
        });

        it('with incorrect body type.', function() {
            this.fakeHTTP.req = incorrectBody;

            validator.isBodyObject(this.fakeHTTP.req, this.fakeHTTP.res, this.fakeHTTP.next);

            assert.isFalse(this.fakeHTTP.next.called);
        });

        it('without body.', function() {
            validator.isBodyObject(this.fakeHTTP.req, this.fakeHTTP.res, this.fakeHTTP.next);

            assert.isFalse(this.fakeHTTP.next.called);
        });
    });
});