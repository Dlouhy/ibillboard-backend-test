'use strict';

const redis = new (require('../storage/Redis'))();

module.exports = {
    get: async (req, res, next) => {
        try {
            const count = await redis.cmd('get', 'count');

            res.count = Number.isNaN(+count) ? 0 : +count;

            return next();
        } catch(e) {
            return res.status(500).json({ error: true, message: e, count: null });
        }
    },
    append: async (req, res, next) => {
        const count = +req.body.count;

        if(Number.isNaN(count)) {
            return next();
        }

        try {
            await redis.cmd('incrbyfloat', 'count', count);

            return next();
        } catch(e) {
            return res.status(500).json({ error: true, message: e });
        }
    }
}