'use strict';

const fs = require('fs');
const path = require('path');
const { promisify } = require('util');
const appendFileAsync = promisify(fs.appendFile);

module.exports = {
    appendRequestBody: async (req, res, next) => {
        const body = JSON.stringify(req.body);

        try {
            const res = await appendFileAsync(path.join(__dirname, '../storage/trackedData.txt'), body);

            return next();
        } catch(error) {
            return res.status(500).json({ error: true, message: error });
        }
    }
}