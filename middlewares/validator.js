'use strict';

module.exports = {
    isBodyObject: (req, res, next) => {
        if(typeof req.body !== 'object') {
            return res.status(403).json({ error: true, message: 'Request body has incorrect format. It should be object.' });
        }

        return next();
    }
}