'use strict';

const assert = require('chai').assert;
const sinon = require('sinon');
const path = require('path');
const util = require('util');

const prepareFakeHTTP = require('../tests/prepareFakeHTTP');

const appendDataFilePath = path.join(__dirname, '../storage/trackedData.txt');
const dataForSuccessCallback = {};
const dataForFailCallback = {error: true};

describe('file middleware', function() {
    describe('uses method appendRequestBody', function() {
        before(function () {
            sinon.stub(util, 'promisify').callsFake(() => {
                const appendFile = sinon.stub();

                appendFile.withArgs(appendDataFilePath, JSON.stringify(dataForSuccessCallback)).resolves(true);
                appendFile.withArgs(appendDataFilePath, JSON.stringify(dataForFailCallback)).rejects(false);

                return appendFile;
            });

            this.file = require('./file');
        });

        it('and successfully appends data', async function() {
            const fakeHTTP = prepareFakeHTTP(dataForSuccessCallback);

            await this.file.appendRequestBody(fakeHTTP.req, fakeHTTP.res, fakeHTTP.next);

            assert.isTrue(fakeHTTP.next.called);
        });

        it('and unsuccessfully appends data', async function() {
            const fakeHTTP = prepareFakeHTTP(dataForFailCallback);

            await this.file.appendRequestBody(fakeHTTP.req, fakeHTTP.res, fakeHTTP.next);

            assert.isFalse(fakeHTTP.next.called);
        });
    });
});