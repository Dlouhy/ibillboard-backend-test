'use strict';

const assert = require('chai').assert;
const sinon = require('sinon');
const proxyquire = require('proxyquire');

const prepareFakeHTTP = require('../tests/prepareFakeHTTP');

const correctNumber = 5;
const incorrectNumber = 'Hello';

describe('redisCount middleware', function() {
    before(function () {
        const redisCmdStub = this.sandbox.stub();
        this.redisCmdStub = redisCmdStub;

        this.redisCount = proxyquire('./redisCount', {
            '../storage/Redis': function() {
                return {
                    cmd: redisCmdStub
                }
            }
        });
    });

    afterEach(function() {
        this.redisCmdStub.reset();
    });

    describe('uses method get', function() {
        beforeEach(function() {
            this.fakeHTTP = prepareFakeHTTP();
        });

        it('and successfully gets stored data.', async function() {
            this.redisCmdStub.returns(correctNumber);

            await this.redisCount.get(this.fakeHTTP.req, this.fakeHTTP.res, this.fakeHTTP.next);

            assert.isTrue(this.fakeHTTP.next.called);
            assert.equal(this.fakeHTTP.res.count, correctNumber);
        });

        it('and gets non-number value from redis.', async function() {
            this.redisCmdStub.returns(incorrectNumber);

            await this.redisCount.get(this.fakeHTTP.req, this.fakeHTTP.res, this.fakeHTTP.next);

            assert.isTrue(this.fakeHTTP.next.called);
            assert.notEqual(this.fakeHTTP.res.count, correctNumber);
            assert.equal(this.fakeHTTP.res.count, 0);
        });

        it('and redis throws error.', async function() {
            this.redisCmdStub.throws();

            await this.redisCount.get(this.fakeHTTP.req, this.fakeHTTP.res, this.fakeHTTP.next);

            assert.isFalse(this.fakeHTTP.next.called);
            assert.notExists(this.fakeHTTP.res.count);
        });
    });

    describe('uses method append', function() {
        it('and successfully appends data.', async function() {
            const fakeHTTP = prepareFakeHTTP({ count: correctNumber });

            this.redisCmdStub.rejects();
            this.redisCmdStub.withArgs('incrbyfloat', 'count', correctNumber).resolves(true);

            await this.redisCount.append(fakeHTTP.req, fakeHTTP.res, fakeHTTP.next);

            assert.isTrue(fakeHTTP.next.called);
            assert.isTrue(this.redisCmdStub.called);
        });

        it('and tries to append non-number value.', async function() {
            const fakeHTTP = prepareFakeHTTP({ count: incorrectNumber });

            await this.redisCount.append(fakeHTTP.req, fakeHTTP.res, fakeHTTP.next);

            assert.isTrue(fakeHTTP.next.called);
            assert.isFalse(this.redisCmdStub.called);
        });

        it('and redis throws error.', async function() {
            const fakeHTTP = prepareFakeHTTP({ count: correctNumber });

            this.redisCmdStub.throws();

            await this.redisCount.append(fakeHTTP.req, fakeHTTP.res, fakeHTTP.next);

            assert.isFalse(fakeHTTP.next.called);
            assert.isTrue(this.redisCmdStub.called);
        });
    });
});