'use strict';

const assert = require('chai').assert;
const sinon = require('sinon');
const redis = require('redis');

const defaultSavedValueInRedis = 0;
const valueForRedis = 5;
const successMessage = 'OK';

describe('Redis wrapper', function() {
    before(function () {
        this.sandbox.stub(redis, 'createClient').returns({
            existing: (value, callback) => {
                this.savedValueInRedis = value;

                return callback(null, successMessage);
            },
            on: this.sandbox.stub()
        });

        this.redisWrapper = new (require('./Redis'))();
    });

    beforeEach(function() {
        this.savedValueInRedis = defaultSavedValueInRedis;
    });

    it('calls existing command', async function() {
        try {
            const response = await this.redisWrapper.cmd('existing', valueForRedis);

            assert.equal(response, successMessage);
            assert.equal(this.savedValueInRedis, valueForRedis);
        } catch(error) {
            throw Error(error);
        }
    });

    it('calls non-existing command', async function() {
        try {
            const response = await this.redisWrapper.cmd('non-existing', valueForRedis);

            assert.notEqual(this.savedValueInRedis, valueForRedis);
        } catch(error) {
            assert.instanceOf(error, Error);
            assert.equal(this.savedValueInRedis, defaultSavedValueInRedis);
        }
    });
});