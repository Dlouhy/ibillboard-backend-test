'use strict';

const redis = require('redis');

class Redis {
    constructor() {
        this.client = redis.createClient({
            host: process.env.REDIS_HOST,
            port: process.env.REDIS_PORT,
        });

        this.client.on("error", (err) => {
            console.log("Redis error ", err);
        });
    }

    async cmd(command, ...args) {
        return new Promise((resolve, reject) => {
            if(this.client[command] instanceof Function === false) {
                return reject(new Error(`Command '${command}' doesn't exist`));
            }

            this.client[command](...args, (error, result) => {
                if (error) {
                    return reject(error);
                }

                return resolve(result);
            });
        });
    }
}

module.exports = Redis;