'use strict';

const router = require('express').Router();

const redisCount = require('../middlewares/redisCount');
const file = require('../middlewares/file');
const validator = require('../middlewares/validator');

/**
 * @api {GET} /count Get redis count
 * @apiName Get redis count
 * @apiGroup MainAPI
 *
 * @apiDescription Gets value with key "count" from redis.
 *
 * @apiSuccess {Boolean} error Tells whether the request was processed with or without errors.
 * @apiSuccess {String}  [message] Error message.
 * @apiSuccess {Number} count Value stored in redis. In case there isn't stored count in Redis, the returned value is 0.
 */
router.get('/count', redisCount.get, (req, res) => {
    return res.json({ error: false, count: res.count });
});


/**
 * @api {POST} /track Track requests
 * @apiName Track requests
 * @apiGroup MainAPI
 *
 * @apiDescription API for getting JSON input, which will be appended to file.
 *
 * @apiParam {Number} [count] Count, which should be appended.
 * @apiParam ...other Body can contain any other data.
 *
 * @apiSuccess {Boolean} error Tells whether the request was processed with or without errors.
 * @apiSuccess {String}  [message] Error message.
 * @apiSuccess {String}  [body] Received data.
 */
router.post('/track', validator.isBodyObject, file.appendRequestBody, redisCount.append, (req, res) => {
    return res.json({ error: false, body: req.body });
});

module.exports = router;