'use strict';

const sinon = require('sinon');

module.exports = function(body = null) {
    const req = body ? { body } : {};

    return {
        req,
        res: {
            status: sinon.stub().returns({ json: (msg) => {} })
        },
        next: sinon.stub()
    };
}