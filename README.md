# Internet Billboard Backend Test

Completed backend test, which is defined [HERE](http://www.ibillboard.com/uloha/vyvojar-uloha-1)

## Getting Started

These instructions will help you get project up and running.

### Prerequisites

You need to have either node, npm and redis installed for native setup or docker engine and docker compose for running in docker machine.

For docker engine follow this URL: [Get Started With Docker](https://www.docker.com/get-started)

For node, npm and Redis follow these URLs: [Get Started With Node](https://nodejs.org/en/), [Get Started With Redis](https://redislabs.com/resources/get-started-with-redis/)

### Configuration

For local development copy .env.dist and its contents to .env

#### App Config

- APP_PORT - port the system will start listening on.

#### Redis Config

- REDIS_HOST - Redis host *(default: 127.0.0.1)*
- REDIS_PORT - Redis port *(default: 6379)*

### Installing

#### Native Setup

First install NPM packages using this command

```
npm install
```

Then make sure you have installed and started Redis service.

Then create a .env file for your local environment, use .env.dist as a template.

Run this command:

```
npm start
```

#### Docker Compose

Run this command:

```
docker-compose build
docker-compose up
```

... or if you want to start the project in background

```
docker-compose build
docker-compose up -d
```

## Testing

### Native Setup

For running tests run this command:

```
npm run test-unit
```

### Docker Compose

For running tests run this command:

```
docker-compose -f docker-compose.test.yml up --build
```

## Built With

* [Express](https://expressjs.com/) - Fast, unopinionated, minimalist web framework for Node.js