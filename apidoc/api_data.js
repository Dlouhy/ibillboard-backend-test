define({ "api": [
  {
    "type": "GET",
    "url": "/count",
    "title": "Get redis count",
    "name": "Get_redis_count",
    "group": "MainAPI",
    "description": "<p>Gets value with key &quot;count&quot; from redis.</p>",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "error",
            "description": "<p>Tells whether the request was processed with or without errors.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": true,
            "field": "message",
            "description": "<p>Error message.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "count",
            "description": "<p>Value stored in redis. In case there isn't stored count in Redis, the returned value is 0.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "routes/index.js",
    "groupTitle": "MainAPI"
  },
  {
    "type": "POST",
    "url": "/track",
    "title": "Track requests",
    "name": "Track_requests",
    "group": "MainAPI",
    "description": "<p>API for getting JSON input, which will be appended to file.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "count",
            "description": "<p>Count, which should be appended.</p>"
          },
          {
            "group": "Parameter",
            "optional": false,
            "field": "...other",
            "description": "<p>Body can contain any other data.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "error",
            "description": "<p>Tells whether the request was processed with or without errors.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": true,
            "field": "message",
            "description": "<p>Error message.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": true,
            "field": "body",
            "description": "<p>Received data.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "routes/index.js",
    "groupTitle": "MainAPI"
  }
] });
