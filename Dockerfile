FROM node:10-alpine

# Create app directory
WORKDIR /usr/app

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY package*.json ./

# Install npm packages
RUN npm install

# Bundle app source
COPY . .

# Remove local environment variables
RUN rm -rf .env

EXPOSE 3000

CMD [ "npm", "start" ]